package enigma.taskOne.data.helper;

import enigma.taskOne.enums.EnumLeafyTreeTypes;
import enigma.taskOne.interfaces.ILeafyTreeType;

public enum LeafyTreeTypeHelper implements ILeafyTreeType {

    INSTANCE;

    @Override
    public EnumLeafyTreeTypes receiveLeafyTreeType(final int drawnNumber) {
        switch (drawnNumber) {
            case 0 -> {
                return EnumLeafyTreeTypes.OAK;
            }
            case 1 -> {
                return EnumLeafyTreeTypes.BEECH;
            }
            case 2 -> {
                return EnumLeafyTreeTypes.CLONE;
            }
            case 3 -> {
                return EnumLeafyTreeTypes.BIRCH;
            }
            case 4 -> {
                return EnumLeafyTreeTypes.ROWAN;
            }
            case 5 -> {
                return EnumLeafyTreeTypes.BIRD_CHERRY;
            }
            case 6 -> {
                return EnumLeafyTreeTypes.APPLE_TREE;
            }
            case 7 -> {
                return EnumLeafyTreeTypes.ALDER;
            }
            case 8 -> {
                return EnumLeafyTreeTypes.HORNBEAM;
            }
            case 9 -> {
                return EnumLeafyTreeTypes.POPLAR;
            }
            case 10 -> {
                return EnumLeafyTreeTypes.HAWTHORN;
            }
            case 11 -> {
                return EnumLeafyTreeTypes.LIME_TREE;
            }
            case 12 -> {
                return EnumLeafyTreeTypes.WILLOW;
            }
            case 13 -> {
                return EnumLeafyTreeTypes.ELM;
            }
            case 14 -> {
                return EnumLeafyTreeTypes.CHERRY;
            }
            case 15 -> {
                return EnumLeafyTreeTypes.ASH;
            }
            default -> {
                return null;
            }
        }
    }
}
