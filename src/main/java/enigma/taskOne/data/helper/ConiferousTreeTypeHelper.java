package enigma.taskOne.data.helper;

import enigma.taskOne.enums.EnumConiferousTreeTypes;
import enigma.taskOne.interfaces.IConiferousTreeType;

public enum ConiferousTreeTypeHelper implements IConiferousTreeType {

    INSTANCE;

    @Override
    public EnumConiferousTreeTypes receiveConiferousTreeType(final int drawnNumber) {
        switch (drawnNumber) {
            case 0 -> {
                return EnumConiferousTreeTypes.LARCH;
            }
            case 1 -> {
                return EnumConiferousTreeTypes.MARSH_CYPRESS;
            }
            case 2 -> {
                return EnumConiferousTreeTypes.CHINESE_META_SEQUOIA;
            }
            case 3 -> {
                return EnumConiferousTreeTypes.SPRUCE;
            }
            case 4 -> {
                return EnumConiferousTreeTypes.NOBLE_FIR;
            }
            case 5 -> {
                return EnumConiferousTreeTypes.CAUCASIAN_FIR;
            }
            case 6 -> {
                return EnumConiferousTreeTypes.KOREAN_FIR;
            }
            case 7 -> {
                return EnumConiferousTreeTypes.BLACK_PINE;
            }
            case 8 -> {
                return EnumConiferousTreeTypes.HIMALAYAN_PINE;
            }
            case 9 -> {
                return EnumConiferousTreeTypes.MOUNTAIN_PINE;
            }
            case 10 -> {
                return EnumConiferousTreeTypes.NOOTKA_CYPRESS;
            }
            default -> {
                return null;
            }
        }
    }
}
