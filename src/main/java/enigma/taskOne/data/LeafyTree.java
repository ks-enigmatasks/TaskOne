package enigma.taskOne.data;

import enigma.taskOne.enums.EnumLeafyTreeTypes;
import enigma.taskOne.exceptions.CannotTrimBranchesException;
import enigma.taskOne.exceptions.CannotTrimLeavesOrNeedlesException;
import enigma.taskOne.interfaces.ITreeDevelopment;
import enigma.taskOne.interfaces.ITreeSeedling;
import lombok.Getter;

public class LeafyTree extends Tree {

    private final int LEAVES_COUNT = 4;
    @Getter
    private int leaves;
    @Getter
    private final EnumLeafyTreeTypes treeType;

    public LeafyTree() {
        branches = ITreeSeedling.INITIAL_BRANCHES;
        root = ITreeSeedling.INITIAL_ROOT;
        trunk = ITreeSeedling.INITIAL_TRUNK;
        leaves = ITreeSeedling.INITIAL_NUMBER_OF_LEAVES_OR_NEEDLES;
        treeType = randomLeafyTreeType();
    }

    @Override
    public void dropTreeCoverForTheWinter() {
        leaves = ITreeDevelopment.NUMBER_OF_LEAVES_OR_NEEDLES_AFTER_WINTER;
    }

    @Override
    public void grow() {
        branches += ITreeDevelopment.GROW_BRANCHES_COUNT;
        if (leaves == ITreeDevelopment.NUMBER_OF_LEAVES_OR_NEEDLES_AFTER_WINTER) {
            leaves = ITreeSeedling.INITIAL_NUMBER_OF_LEAVES_OR_NEEDLES;
        }
        leaves *= LEAVES_COUNT;
        root += ITreeDevelopment.GROW_ROOT_COUNT;
    }

    @Override
    public void trim() throws CannotTrimLeavesOrNeedlesException, CannotTrimBranchesException {
        if (canBeTrim(branches)) {
            branches -= ITreeDevelopment.TRIM_BRANCHES_COUNT;
            if (leaves >= LEAVES_COUNT) {
                leaves /= LEAVES_COUNT;
            } else {
                throw new CannotTrimLeavesOrNeedlesException("The tree cannot be trimmed because it has too few leaves");
            }
        } else
            throw new CannotTrimBranchesException("The tree cannot be trimmed because it has too few branches");
    }

    @Override
    public void cutDown() {
        super.cutDown();
        leaves = ITreeDevelopment.CUT_TREE;
    }
}
