package enigma.taskOne.data;

import enigma.taskOne.enums.EnumConiferousTreeTypes;
import enigma.taskOne.exceptions.CannotTrimBranchesException;
import enigma.taskOne.exceptions.CannotTrimLeavesOrNeedlesException;
import enigma.taskOne.interfaces.ITreeDevelopment;
import enigma.taskOne.interfaces.ITreeSeedling;
import lombok.Getter;

public class Conifer extends Tree {

    private final int NEEDLES_COUNT = 6;
    @Getter
    private int needles;
    @Getter
    private final EnumConiferousTreeTypes treeType;

    public Conifer() {
        branches = ITreeSeedling.INITIAL_BRANCHES;
        root = ITreeSeedling.INITIAL_ROOT;
        trunk = ITreeSeedling.INITIAL_TRUNK;
        needles = ITreeSeedling.INITIAL_NUMBER_OF_LEAVES_OR_NEEDLES;
        treeType = randomConiferousTreeType();
    }

    public Conifer(final EnumConiferousTreeTypes coniferousType) {
        branches = ITreeSeedling.INITIAL_BRANCHES;
        root = ITreeSeedling.INITIAL_ROOT;
        trunk = ITreeSeedling.INITIAL_TRUNK;
        needles = ITreeSeedling.INITIAL_NUMBER_OF_LEAVES_OR_NEEDLES;
        treeType = coniferousType;
    }

    @Override
    public void dropTreeCoverForTheWinter() {
        if (treeType.isShedNeedles()) {
            needles = ITreeDevelopment.NUMBER_OF_LEAVES_OR_NEEDLES_AFTER_WINTER;
        }
    }

    @Override
    public void grow() {
        branches += ITreeDevelopment.GROW_BRANCHES_COUNT;
        if (needles == ITreeDevelopment.NUMBER_OF_LEAVES_OR_NEEDLES_AFTER_WINTER) {
            needles = ITreeSeedling.INITIAL_NUMBER_OF_LEAVES_OR_NEEDLES;
        }
        needles *= NEEDLES_COUNT;
        root += ITreeDevelopment.GROW_ROOT_COUNT;
    }

    @Override
    public void trim() throws CannotTrimLeavesOrNeedlesException, CannotTrimBranchesException {
        if (canBeTrim(branches)) {
            branches -= ITreeDevelopment.TRIM_BRANCHES_COUNT;
            if (needles >= NEEDLES_COUNT) {
                needles /= NEEDLES_COUNT;
            } else {
                throw new CannotTrimLeavesOrNeedlesException("The tree cannot be trimmed because it has too few needles");
            }
        } else throw new CannotTrimBranchesException("The tree cannot be trimmed because it has too few branches");
    }

    @Override
    public void cutDown() {
        super.cutDown();
        needles = ITreeDevelopment.CUT_TREE;
    }
}
