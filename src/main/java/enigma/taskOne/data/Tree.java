package enigma.taskOne.data;

import enigma.taskOne.data.helper.ConiferousTreeTypeHelper;
import enigma.taskOne.data.helper.LeafyTreeTypeHelper;
import enigma.taskOne.enums.EnumConiferousTreeTypes;
import enigma.taskOne.enums.EnumLeafyTreeTypes;
import enigma.taskOne.exceptions.CannotTrimBranchesException;
import enigma.taskOne.exceptions.CannotTrimLeavesOrNeedlesException;
import enigma.taskOne.interfaces.ITreeDevelopment;
import enigma.taskOne.interfaces.ITreeSeedling;
import lombok.Getter;

import java.util.concurrent.ThreadLocalRandom;

public abstract class Tree implements ITreeSeedling, ITreeDevelopment {

    private final int INITIAL_NUMBER = 0;
    private final LeafyTreeTypeHelper leafyTreeType = LeafyTreeTypeHelper.INSTANCE;
    private final ConiferousTreeTypeHelper coniferousTreeType = ConiferousTreeTypeHelper.INSTANCE;
    @Getter
    protected int trunk;
    @Getter
    protected int branches;
    @Getter
    protected int root;

    public EnumLeafyTreeTypes randomLeafyTreeType() {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        int rand = random.nextInt(INITIAL_NUMBER, EnumLeafyTreeTypes.values().length);
        return leafyTreeType.receiveLeafyTreeType(rand);
    }

    public EnumConiferousTreeTypes randomConiferousTreeType() {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        int rand = random.nextInt(INITIAL_NUMBER, EnumConiferousTreeTypes.values().length);
        return coniferousTreeType.receiveConiferousTreeType(rand);
    }

    boolean canBeTrim(int branch) {
        return (branch >= INITIAL_BRANCHES);
    }

    @Override
    public abstract void dropTreeCoverForTheWinter();

    @Override
    public void cutDown(){
        branches = ITreeDevelopment.CUT_TREE;
        trunk = ITreeDevelopment.CUT_TREE;
    }

    @Override
    public abstract void grow();

    @Override
    public abstract void trim() throws CannotTrimLeavesOrNeedlesException, CannotTrimBranchesException;
}
