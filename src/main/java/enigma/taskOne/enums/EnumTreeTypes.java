package enigma.taskOne.enums;

public enum EnumTreeTypes {
    CONICAl("Conical"),
    COLUMNAR("Columnar"),
    ROUND("Round"),
    UMBRELLA("Umbrella");

    private final String name;

    EnumTreeTypes(final String name) {
        this.name = name;
    }
}
