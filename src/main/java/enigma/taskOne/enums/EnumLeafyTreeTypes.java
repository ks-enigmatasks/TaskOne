package enigma.taskOne.enums;

import lombok.Getter;

public enum EnumLeafyTreeTypes {
    OAK("Oak", EnumTreeTypes.ROUND), BEECH("Beech", EnumTreeTypes.ROUND),
    CLONE("Clone", EnumTreeTypes.ROUND), BIRCH("Birch", EnumTreeTypes.CONICAl),
    ROWAN("Rowan", EnumTreeTypes.UMBRELLA), BIRD_CHERRY("Bird cherry", EnumTreeTypes.ROUND),
    APPLE_TREE("Apple tree", EnumTreeTypes.UMBRELLA), ALDER("Alder", EnumTreeTypes.ROUND),
    HORNBEAM("Hornbeam", EnumTreeTypes.ROUND), POPLAR("Poplar", EnumTreeTypes.COLUMNAR),
    HAWTHORN("Hawthorn", EnumTreeTypes.UMBRELLA), LIME_TREE("Lime tree", EnumTreeTypes.ROUND),
    WILLOW("Willow", EnumTreeTypes.ROUND), ELM("Elm", EnumTreeTypes.UMBRELLA),
    CHERRY("Cherry", EnumTreeTypes.UMBRELLA), ASH("Ash", EnumTreeTypes.ROUND);

    @Getter
    private final String name;
    @Getter
    private final EnumTreeTypes treeType;

    EnumLeafyTreeTypes(final String name, final EnumTreeTypes treeType) {
        this.name = name;
        this.treeType = treeType;
    }
}
