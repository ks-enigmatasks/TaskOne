package enigma.taskOne.enums;

import lombok.Getter;

public enum EnumConiferousTreeTypes {
    LARCH("Larch", EnumTreeTypes.COLUMNAR, true),
    MARSH_CYPRESS("Marsh cypress", EnumTreeTypes.CONICAl, true),
    CHINESE_META_SEQUOIA("Chinese meta-sequoia", EnumTreeTypes.CONICAl, true),
    SPRUCE("Spruce", EnumTreeTypes.CONICAl, false),
    NOBLE_FIR("Noble fir", EnumTreeTypes.CONICAl, false),
    CAUCASIAN_FIR("Caucasian fir", EnumTreeTypes.CONICAl, false),
    KOREAN_FIR("Korean fir", EnumTreeTypes.CONICAl, false),
    BLACK_PINE("Black pine", EnumTreeTypes.ROUND, false),
    HIMALAYAN_PINE("Himalayan pine", EnumTreeTypes.ROUND, false),
    MOUNTAIN_PINE("Mountain pine", EnumTreeTypes.CONICAl, false),
    NOOTKA_CYPRESS("Nootka cypress", EnumTreeTypes.CONICAl, false);

    @Getter
    private final String name;
    @Getter
    private final EnumTreeTypes treeType;
    @Getter
    private final boolean shedNeedles;

    EnumConiferousTreeTypes(final String name, final EnumTreeTypes treeType, final boolean shedNeedles) {
        this.name = name;
        this.treeType = treeType;
        this.shedNeedles = shedNeedles;
    }
}
