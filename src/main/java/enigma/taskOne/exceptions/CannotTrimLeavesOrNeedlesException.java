package enigma.taskOne.exceptions;

public class CannotTrimLeavesOrNeedlesException extends Exception {

    public CannotTrimLeavesOrNeedlesException(final String message) {
        super(message);
    }
}
