package enigma.taskOne.exceptions;

public class CannotTrimBranchesException extends Exception {

    public CannotTrimBranchesException(final String message) {
        super(message);
    }
}
