package enigma.taskOne.interfaces;

import enigma.taskOne.enums.EnumLeafyTreeTypes;

public interface ILeafyTreeType {

    EnumLeafyTreeTypes receiveLeafyTreeType(final int drawnNumber);
}
