package enigma.taskOne.interfaces;

public interface ITreeSeedling {
    int INITIAL_NUMBER_OF_LEAVES_OR_NEEDLES = 1;
    int INITIAL_BRANCHES = 3;
    int INITIAL_ROOT = 1;
    int INITIAL_TRUNK = 1;
}
