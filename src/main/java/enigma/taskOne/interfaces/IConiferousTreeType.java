package enigma.taskOne.interfaces;

import enigma.taskOne.enums.EnumConiferousTreeTypes;

public interface IConiferousTreeType {

    EnumConiferousTreeTypes receiveConiferousTreeType(final int drawnNumber);
}
