package enigma.taskOne.interfaces;

import enigma.taskOne.exceptions.CannotTrimBranchesException;
import enigma.taskOne.exceptions.CannotTrimLeavesOrNeedlesException;

public interface ITreeDevelopment {
    int NUMBER_OF_LEAVES_OR_NEEDLES_AFTER_WINTER = 0;
    int GROW_BRANCHES_COUNT = 2;
    int TRIM_BRANCHES_COUNT = 3;
    int GROW_ROOT_COUNT = 1;
    int CUT_TREE = 0;

    void dropTreeCoverForTheWinter();

    void grow();

    void trim() throws CannotTrimLeavesOrNeedlesException, CannotTrimBranchesException;

    void cutDown();
}
