package enigma.taskOne.data;

import enigma.taskOne.enums.EnumConiferousTreeTypes;
import enigma.taskOne.exceptions.CannotTrimBranchesException;
import enigma.taskOne.exceptions.CannotTrimLeavesOrNeedlesException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ConiferTest {

    private final int SHED_NEEDLES_COUNT = 0;
    private final int NOT_SHED_NEEDLES_COUNT = 1;
    private final int NEEDLES_AFTER_TRIM = 1;
    private final int BRANCHES_AFTER_TRIM = 3;
    private final int CUT_DOWN_TREE = 0;

    private final Conifer testConifer = new Conifer();
    private final Conifer testConiferTwo = new Conifer();

    private static final List<Conifer> coniferTrees = List.of((new Conifer(EnumConiferousTreeTypes.LARCH)),
            (new Conifer(EnumConiferousTreeTypes.MARSH_CYPRESS)), (new Conifer(EnumConiferousTreeTypes.CHINESE_META_SEQUOIA)),
            (new Conifer(EnumConiferousTreeTypes.SPRUCE)), (new Conifer(EnumConiferousTreeTypes.NOBLE_FIR)),
            (new Conifer(EnumConiferousTreeTypes.CAUCASIAN_FIR)), (new Conifer(EnumConiferousTreeTypes.KOREAN_FIR)),
            (new Conifer(EnumConiferousTreeTypes.BLACK_PINE)), (new Conifer(EnumConiferousTreeTypes.HIMALAYAN_PINE)),
            (new Conifer(EnumConiferousTreeTypes.MOUNTAIN_PINE)), (new Conifer(EnumConiferousTreeTypes.NOOTKA_CYPRESS)));

    private static Stream<Conifer> treesThatShedTheirNeedlesInWinter() {
        return coniferTrees.stream().filter(tree -> tree.getTreeType().isShedNeedles());
    }

    private static Stream<Conifer> treesThatDontShedTheirNeedlesInWinter() {
        return coniferTrees.stream().filter(tree -> !tree.getTreeType().isShedNeedles());
    }

    @ParameterizedTest
    @MethodSource("treesThatShedTheirNeedlesInWinter")
    @DisplayName("shouldDropTheCoverForWinterWhenConiferousTreeShedNeedles")
    void dropTreeCoverForTheWinter_coniferousTreeTypeShedNeedles(final Conifer conifer) {
        //given

        //when
        conifer.dropTreeCoverForTheWinter();

        //then
        assertEquals(SHED_NEEDLES_COUNT, conifer.getNeedles());
    }

    @ParameterizedTest
    @MethodSource("treesThatDontShedTheirNeedlesInWinter")
    @DisplayName("shouldDontDropTheCoverForWinterWhenConiferousTreeTypeDontShedNeedles")
    void dropTreeCoverForTheWinter_coniferousTreeTypeDontShedNeedles(final Conifer conifer) {
        //given

        //when
        conifer.dropTreeCoverForTheWinter();

        //then
        assertEquals(NOT_SHED_NEEDLES_COUNT, conifer.getNeedles());
    }

    @Test
    @DisplayName("shouldGrowTreeIfEverythingIsOk")
    void grow_correctlyGrow() {
        //given

        //when
        testConifer.grow();

        //then
        assertNotEquals(testConifer.getNeedles(), testConiferTwo.getNeedles());
        assertNotEquals(testConifer.getRoot(), testConiferTwo.getRoot());
        assertNotEquals(testConifer.getBranches(), testConiferTwo.getBranches());
    }

    @Test
    @DisplayName("shouldTrimTreeIfAllIsWell")
    void trim_correctlyTrim() throws CannotTrimBranchesException, CannotTrimLeavesOrNeedlesException {
        //given
        testConifer.grow();

        //when
        testConifer.trim();

        //then
        assertEquals(NEEDLES_AFTER_TRIM, testConifer.getNeedles());
        assertNotEquals(BRANCHES_AFTER_TRIM, testConifer.getBranches());
    }

    @Test
    @DisplayName("shouldThrowCannotTrimBranchesExceptionWhenNotEnoughBranchesToCut")
    void trim_notEnoughBranches() throws CannotTrimBranchesException, CannotTrimLeavesOrNeedlesException {
        //given
        testConifer.grow();
        testConifer.trim();

        //when+then
        assertThrows(CannotTrimBranchesException.class, testConifer::trim);
    }

    @Test
    @DisplayName("shouldThrowCannotTrimLeavesOrNeedlesExceptionWhenNotEnoughNeedlesToCut")
    void trim_notEnoughNeedles() {
        //given

        //when+then
        assertThrows(CannotTrimLeavesOrNeedlesException.class, testConifer::trim);
    }

    @Test
    @DisplayName("shouldCutDownTree")
    void cutDown_correctlyCutDownTree() {
        //given

        //when
        testConifer.cutDown();

        //then
        assertEquals(CUT_DOWN_TREE, testConifer.getNeedles());
        assertEquals(CUT_DOWN_TREE, testConifer.getBranches());
        assertEquals(CUT_DOWN_TREE, testConifer.getTrunk());
    }

}