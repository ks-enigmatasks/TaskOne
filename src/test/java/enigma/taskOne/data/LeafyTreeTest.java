package enigma.taskOne.data;

import enigma.taskOne.exceptions.CannotTrimBranchesException;
import enigma.taskOne.exceptions.CannotTrimLeavesOrNeedlesException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LeafyTreeTest {

    private final int LEAVES_AFTER_WINTER = 0;
    private final int LEAVES_AFTER_TRIM =1;
    private final int BRANCHES_AFTER_TRIM = 3;
    private final int CUT_DOWN_TREE = 0;

    private final LeafyTree testLeafyTree = new LeafyTree();
    private final LeafyTree testLeafyTreeTwo = new LeafyTree();

    @Test
    @DisplayName("shouldDropTheCoverForWinter")
    void dropTreeCoverForTheWinter_allTreesDropLeaves() {
        //given

        //when
        testLeafyTree.dropTreeCoverForTheWinter();

        //then
        assertEquals(LEAVES_AFTER_WINTER, testLeafyTree.getLeaves());
    }

    @Test
    @DisplayName("shouldGrowTreeIfEverythingIsOk")
    void grow_correctlyGrow() {
        //given

        //when
        testLeafyTree.grow();

        //then
        assertNotEquals(testLeafyTree.getLeaves(), testLeafyTreeTwo.getLeaves());
        assertNotEquals(testLeafyTree.getRoot(), testLeafyTreeTwo.getRoot());
        assertNotEquals(testLeafyTree.getBranches(), testLeafyTreeTwo.getBranches());
    }

    @Test
    @DisplayName("shouldTrimTreeIfAllIsWell")
    void trim_correctlyTrim() throws CannotTrimLeavesOrNeedlesException, CannotTrimBranchesException {
        //given
        testLeafyTree.grow();

        //when
        testLeafyTree.trim();

        //then
        assertEquals(LEAVES_AFTER_TRIM, testLeafyTree.getLeaves());
        assertNotEquals(BRANCHES_AFTER_TRIM, testLeafyTree.getBranches());
    }

    @Test
    @DisplayName("shouldThrowCannotTrimBranchesExceptionWhenNotEnoughBranchesToCut")
    void trim_notEnoughBranches() throws CannotTrimBranchesException, CannotTrimLeavesOrNeedlesException {
        //given
        testLeafyTree.grow();
        testLeafyTree.trim();

        //when+then
        assertThrows(CannotTrimBranchesException.class, testLeafyTree::trim);
    }

    @Test
    @DisplayName("shouldThrowCannotTrimLeavesOrNeedlesExceptionWhenNotEnoughLeavesToCut")
    void trim_notEnoughNeedles() {
        //given

        //when+then
        assertThrows(CannotTrimLeavesOrNeedlesException.class, testLeafyTree::trim);
    }

    @Test
    @DisplayName("shouldCutDownTree")
    void cutDown_correctlyCutDownTree() {
        //given

        //when
        testLeafyTree.cutDown();

        //then
        assertEquals(CUT_DOWN_TREE, testLeafyTree.getLeaves());
        assertEquals(CUT_DOWN_TREE, testLeafyTree.getBranches());
    }
}