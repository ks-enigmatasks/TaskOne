# Task One - Tree Class Abstraction

***

## Table of contents

* [General info](#general-info)
  * [Features](#features)
* [Technologies](#technologies)
* [Setup](#setup)

* [Author](#author)

## General info

This project is intended to implement an abstraction of leafy tree and conifer classes.<br>
OOP model of ordinary natural trees. Each tree has a trunk, branches and leaves
and a method to grow, etc.

### Features

* Forms leafy and coniferous trees
* Seedlings are randomly selected from among the available trees<br>
  16 leafy trees and 11 conifers to choose from
* Each tree is shaped according to its type
* Some conifers shed their needles in the winter

## Technologies

Project is created with:

* IntelliJ IDEA 2022.2.3 (Ultimate Edition)
* Java JDK version 17.0.2 [Link][1]
* Maven version 3.8.6 [Link][2]
* org.junit.jupiter version 5.9.0
* org.projectlombok version 1.18.24

## Setup

To run this project, install Java JDK from the link [JDK][1]

Install it locally using cmd:

* $ cd ../selectedDirectory
* $ git clone https://gitlab.com/ks-enigmatasks/TaskOne.git (with https)
* $ git clone git@gitlab.com:ks-enigmatasks/TaskOne.git (with ssh)<br>

Open project in your IDE (Intellij recommended).<br>
Reload Maven to install required libraries.<br>
Note: The project uses external libraries that are downloaded from an external maven repository.

## Author

Kamil Stencel

[1]:https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html
[2]:https://dlcdn.apache.org/maven/maven-3/3.8.6/binaries/apache-maven-3.8.6-bin.zip